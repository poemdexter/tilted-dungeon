﻿using UnityEngine;
using System.Collections;

public class BulletFired : MonoBehaviour
{
    public float speed;
    public float facing;

    private void Update()
    {
        transform.Translate(facing * transform.right * Time.deltaTime * speed);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Platform"))
        {
            Destroy(gameObject);
        }
    }
}