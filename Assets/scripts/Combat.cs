﻿using UnityEngine;
using System.Collections;

public class Combat : MonoBehaviour
{
    public UILabel hpLabel;
    public ParticleSystem bloodyParticles;

    private int HP;

    private void Start()
    {
        HP = 100;
    }

    private void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("bullet"))
        {
            ParticleSystem blood = (ParticleSystem) Instantiate(bloodyParticles,transform.position,Quaternion.identity);
            HP -= 15;
            hpLabel.text = "HP:" + HP;
            if (HP <= 0)
                Died();
            Destroy(col.gameObject);    
        }

        if (col.gameObject.CompareTag("GameController")) // death from falling
        {
            Died();
        }
    }

    private void Died()
    {
        if (CompareTag("Player1"))
        {
            transform.position = new Vector3(-7f, -2.5f, 0);
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().Score(tag);
        }
        if (CompareTag("Player2"))
        {
            transform.position = new Vector3(7f, -2.5f, 0);
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().Score(tag);
        }
        HP = 100;
        hpLabel.text = "HP:" + HP;
    }

    public void Reset()
    {
        if (CompareTag("Player1"))
        {
            transform.position = new Vector3(-7f, -2.5f, 0);
        }
        if (CompareTag("Player2"))
        {
            transform.position = new Vector3(7f, -2.5f, 0);
        }
        HP = 100;
        hpLabel.text = "HP:" + HP;
    }
}