﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public UILabel p1Text, p2Text, title, winner;
    private int p1Score, p2Score;
    public Shoot p1Shoot, p2Shoot;
    public Movement p1Move, p2Move;
    private bool ready;


    public void Score(string playerTag)
    {
        if (ready)
        {
            if (playerTag.Equals("Player1"))
            {
                p2Score++;
                p2Text.text = "KILLS:" + p2Score;
            }

            if (playerTag.Equals("Player2"))
            {
                p1Score++;
                p1Text.text = "KILLS:" + p1Score;
            }

            if (p1Score == 10)
            {
                Win(1);
            }
            if (p2Score == 10)
            {
                Win(2);
            }
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Ready();
        }
    }

    public void Ready()
    {
        if (!ready)
        {
            
            GameObject.FindGameObjectWithTag("Player1").GetComponent<Combat>().Reset();
            GameObject.FindGameObjectWithTag("Player2").GetComponent<Combat>().Reset();
            p1Score = 0;
            p2Score = 0;
            p1Text.text = "KILLS:" + p1Score;
            p2Text.text = "KILLS:" + p2Score;
            ready = true;
            winner.enabled = false;
            title.enabled = false;
            p1Shoot.ready = true;
            p2Shoot.ready = true;
            p1Move.ready = true;
            p2Move.ready = true;
        }
    }

    public void UnReady()
    {
        ready = false;
        
        p1Shoot.ready = false;
        p2Shoot.ready = false;
        p1Move.ready = false;
        p2Move.ready = false;
    }

    private void Win(int team)
    {
        if (team == 1)
            winner.text = "RED TEAM WINS!";
        else if (team == 2)
            winner.text = " TEAM WINS!";

        winner.enabled = true;
        UnReady();
    }
}