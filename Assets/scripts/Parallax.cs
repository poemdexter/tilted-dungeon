﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour
{
    public Transform backLayer, frontLayer;
    private Vector3 p1Pos, p2Pos;

    private void Update()
    {
        Vector3 sum = p1Pos + p2Pos;
        sum = -sum;
        backLayer.position = sum*.1f;
        frontLayer.position = sum*.2f;
    }

    public void UpdatePos(string playerTag, Vector3 position)
    {
        if (playerTag.Equals("Player1"))
            p1Pos = position;
        if (playerTag.Equals("Player2"))
            p2Pos = position;
    }
}